package com.ranull.preventitemdestruction.commands;

import com.ranull.preventitemdestruction.PreventItemDestruction;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PreventItemDestructionCommand implements CommandExecutor {
    private PreventItemDestruction plugin;

    public PreventItemDestructionCommand(PreventItemDestruction plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.1";
        String author = "Ranull";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.AQUA + "PreventItemDestruction " + ChatColor.GRAY
                    + "v" + version);
            sender.sendMessage(ChatColor.GRAY + "/preventitemdestruction " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                    + " Plugin info");

            if (sender.hasPermission("preventitemdestruction.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/preventitemdestruction reload " + ChatColor.DARK_GRAY + "-"
                        + ChatColor.RESET + " Reload plugin");
            }

            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);

            return true;
        }

        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("preventitemdestruction.reload")) {
                sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "PreventItemDestruction"
                        + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " No Permission!");

                return true;
            }

            plugin.reloadConfig();

            sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "PreventItemDestruction"
                    + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Reloaded config file!");
        }

        return true;
    }
}
