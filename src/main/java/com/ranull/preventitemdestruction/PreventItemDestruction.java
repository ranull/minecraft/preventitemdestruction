package com.ranull.preventitemdestruction;

import com.ranull.preventitemdestruction.commands.PreventItemDestructionCommand;
import com.ranull.preventitemdestruction.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public class PreventItemDestruction extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        this.getCommand("preventitemdestruction").setExecutor(new PreventItemDestructionCommand(this));
        this.getServer().getPluginManager().registerEvents(new Events(this), this);
    }
}