package com.ranull.preventitemdestruction.events;

import com.ranull.preventitemdestruction.PreventItemDestruction;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Events implements Listener {
    private PreventItemDestruction plugin;

    public Events(PreventItemDestruction plugin) {
        this.plugin = plugin;
    }

    public List<String> getMaterials(String event) {
        List<String> material = plugin.getConfig().getStringList("prevent." + event);
        return material;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemDamage(EntityDamageEvent event) {
        if (!event.getEntity().getType().equals(EntityType.DROPPED_ITEM)) {
            return;
        }

        if (event.getEntity() instanceof Item) {
            ItemStack item = ((Item) event.getEntity()).getItemStack();
            String material = item.getType().toString();

            if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION)) {
                List<String> explosion = getMaterials("explosion");
                if (explosion.contains(material) || explosion.contains("ALL")) {
                    event.setCancelled(true);
                }
            }

            if (event.getCause().equals(EntityDamageEvent.DamageCause.FIRE)
                    || event.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)) {
                List<String> fire = getMaterials("fire");
                if (fire.contains(material) || fire.contains("ALL")) {
                    event.setCancelled(true);
                }
            }

            if (event.getCause().equals(EntityDamageEvent.DamageCause.LAVA)) {
                List<String> lava = getMaterials("com.random.discordmpdbot.lava");
                if (lava.contains(material) || lava.contains("ALL")) {
                    event.setCancelled(true);
                }
            }

            if (event.getCause().equals(EntityDamageEvent.DamageCause.LIGHTNING)) {
                List<String> lightning = getMaterials("lightning");
                if (lightning.contains(material) || lightning.contains("ALL")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
